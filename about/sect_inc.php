<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="lc_block">
   <h2>Компании</h2>
   <?
   $APPLICATION->IncludeComponent("bitrix:menu", "left_menu", array(
       "ROOT_MENU_TYPE" => "left",
       "MENU_CACHE_TYPE" => "N",
       "MENU_CACHE_TIME" => "36000000",
       "MENU_CACHE_USE_GROUPS" => "Y",
       "MENU_CACHE_GET_VARS" => array(
       ),
       "MAX_LEVEL" => "1",
       "CHILD_MENU_TYPE" => "left",
       "USE_EXT" => "Y",
       "DELAY" => "N",
       "ALLOW_MULTI_SELECT" => "Y",
       "VISIBLE_COUNT" => 30
           ), false
   );
   ?>
</div><!--lc_block-->
<?
$APPLICATION->IncludeComponent("bitrix:advertising.banner", "", array(
    "TYPE" => "ADD_COMPANY_BIG",
    "NOINDEX" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "0",
    "CACHE_NOTES" => ""
        )
);
?>
<!--<div class="lc_block">
   <div class="left_banner_185">
      <img src="/uploads/banner_185x320.jpg" alt="">
   </div>
</div>-->
<!--lc_block-->
