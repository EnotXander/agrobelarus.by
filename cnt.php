<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
global $DB;
CModule::IncludeModule("iblock");
$per_page = 20;

function getUserBitrixByIdEnergo($userId)
{
	$filter = Array("UF_ID" => $userId);
	$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $filter); // выбираем пользователей
	if($arUser = $rsUsers->GetNext())
		return $arUser;
	else
		return false;
}

function getElementByExternalId($EX_ID)
{
	$arSelect = Array("ID");
	$arFilter = Array("EXTERNAL_ID"=>$EX_ID);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>2), $arSelect);
	if($ob = $res->GetNext())
		return $ob['ID'];
	else
		return false;
}
function getElementByExternalIdEx($EX_ID, $IblockId)
{
	$arSelect = Array("ID");
	$arFilter = Array("EXTERNAL_ID"=>$EX_ID, "IBLOCK_ID" => $IblockId);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>2), $arSelect);
	if($ob = $res->GetNext())
		return $ob['ID'];
	else
		return false;
}
function getEnumList($arValues, $propId, $IBLOCK_ID)
{
	$arVals = Array();
	foreach ($arValues as $ex_code)
	{
		$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$IBLOCK_ID, "ID"=>$propCode, "EXTERNAL_ID" => $ex_code));
		if($enum_fields = $property_enums->GetNext())
		{
			$arVals[] = Array("VALUE" => $enum_fields["ID"]);
		}
		else
		{
			global $DB;
			$strSql = "
				SELECT * FROM eb_napr WHERE id=".$ex_code;
			$results = $DB->Query($strSql, false, $err_mess.__LINE__);
			$arRows = Array();
			if ($row = $results->Fetch())
			{
				$ibpenum = new CIBlockPropertyEnum;
				if($PropID = $ibpenum->Add(Array('PROPERTY_ID'=>$propId, 'VALUE'=>$row['title_ru'], "EXTERNAL_ID" => $ex_code)))
					$arVals[] = Array("VALUE" => $PropID);
			}
		}
	}
	return $arVals;
}

function GetParentSectId($parentId, $IblockId=27, $table='eb_tovar_razdel')
{
	echo $IblockId . $table;
	//exit();
	$sectId = false;
	global $DB;
	$strSql = "
		SELECT * FROM ".$table." WHERE id=".$parentId;
	$results = $DB->Query($strSql, false, $err_mess.__LINE__);
	if ($row = $results->Fetch())
	{
		$parentSect = false;
		if ($row['parent_id'] > 0)
			$parentSect = GetParentSectId($row['parent_id'], $IblockId, $table);
		$bs = new CIBlockSection;
		$arFields = Array(
			"ACTIVE" => "Y",
			"IBLOCK_ID" => $IblockId,
			"NAME" => $row['title'],
			"EXTERNAL_ID" => $row['id'],
			"IBLOCK_SECTION_ID" => $parentSect,
		);
		$arFilter = Array('IBLOCK_ID'=>$IblockId, 'EXTERNAL_ID'=>$row['id']);
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFields, false);
		if ($ar_result = $db_list->GetNext())
			$sectId = $ar_result['ID'];
		else
			$sectId = $bs->Add($arFields);
	}
	return $sectId;
}
function GetParentSectIdUs($parentId)
{
	$sectId = false;
	global $DB;
	$strSql = "
		SELECT * FROM eb_uslugi_razdel WHERE id=".$parentId;
	$results = $DB->Query($strSql, false, $err_mess.__LINE__);
	if ($row = $results->Fetch())
	{
		$parentSect = false;
		if ($row['parent_id'] > 0)
			$parentSect = GetParentSectId($row['parent_id']);
		$bs = new CIBlockSection;
		$arFields = Array(
			"ACTIVE" => "Y",
			"IBLOCK_ID" => 28,
			"NAME" => $row['title'],
			"EXTERNAL_ID" => $row['id'],
			"IBLOCK_SECTION_ID" => $parentSect,
		);
		$arFilter = Array('IBLOCK_ID'=>27, 'EXTERNAL_ID'=>$row['id']);
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFields, false);
		if ($ar_result = $db_list->GetNext())
			$sectId = $ar_result['ID'];
		else
			$sectId = $bs->Add($arFields);
	}
	return $sectId;
}
function GetFileProps($table_name, $firm_id)
{
	$prFile = Array();
	$k = 0;
	global $DB;
	$strSql = "
		SELECT * FROM ".$table_name." WHERE firm_id=".$firm_id;
	$results = $DB->Query($strSql, false, $err_mess.__LINE__);
	while ($row = $results->Fetch())
	{
		if (strlen($row['link']) > 0)
		{
			$arF = CFile::MakeFileArray($row['link']);
			$desc = $row['title'];
			$prFile['n'.$k] = Array("VALUE" => $arF, "DESCRIPTION" => $desc);
		}
		else
		{
			$arF = CFile::MakeFileArray("http://energobelarus.by/download/firms/".$firm_id."/".$row['filename']);
			$desc = $row['title'];
			$prFile['n'.$k] = Array("VALUE" => $arF, "DESCRIPTION" => $desc);
		}
		$k++;
	}
	return $prFile;
}
set_time_limit(120);
$arRegionTables = Array("eb_region_", "eb_city_");
if (!isset($_REQUEST['TABLE']))
{
	$strSql = "
		SHOW TABLES FROM bitrix
		";
	$results = $DB->Query($strSql, false, $err_mess.__LINE__);
	$name_array=array();
	$arTables = Array();
	while ($row = $results->Fetch())
	{
		if (stripos($row['Tables_in_bitrix'], "eb_") !== false)
		{
			$arTables[] = $row['Tables_in_bitrix'];
		}
	}
	echo "<form method='GET'><select name='TABLE'>";
	foreach ($arTables as $table)
		echo "<option value='".$table."'>".$table."</option>";
	echo "</select>";
	echo "<br/><input type='submit' value='перейти'></form>";
}
else
{
	$strSql="SELECT count(*) FROM ".$_REQUEST['TABLE'];
	$results = $DB->Query($strSql, false, $err_mess.__LINE__);
	if ($row = $results->Fetch())
	{
		//echo "<pre>"; print_r($row); echo "</pre>";
		$count = $row["count(*)"];
	}
	echo "Количество записей: " . $count;
	if ($_REQUEST['PAGE']>0)
		$page = $_REQUEST['PAGE'];
	else
		$page = 0;
	$strSql = "
		SELECT * FROM ".$_REQUEST['TABLE']." LIMIT ".abs($page*$per_page).",".$per_page;
	$results = $DB->Query($strSql, false, $err_mess.__LINE__);
	$arRows = Array();
	while ($row = $results->Fetch())
	{
		echo "<pre>"; print_r($row); echo "</pre>";
	}
}
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");

?>