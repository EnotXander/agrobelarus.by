<? if(!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') die();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 

global $APPLICATION;
$from = (int)$_REQUEST["from"];

$APPLICATION->IncludeComponent("whipstudio:socialnetwork.messages_chain", "ajax", Array(  
        "PATH_TO_SMILE" => "/bitrix/images/socialnetwork/smile/",
        "SET_TITLE" => "N",
        "PAGE_SIZE" => 999,
        "PAGE" => 1,
        "FROM" => $from,
    )
);