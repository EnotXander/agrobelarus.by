<?if (!$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') die();
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include.php");

$out = array();

if(!CModule::IncludeModule("iblock")) return;

$resCompany = CIBlockElement::GetList(
	array("SORT" => "ASC", "NAME" => "ASC"),
	array(
		"IBLOCK_ID" => $_POST['IBlockId'],
		"ACTIVE" => "Y",
		"NAME" => iconv("UTF-8", "cp1251", $_POST['term']).'%'
	)
);
//$arrCompanies = array();
while ($arrCompany = $resCompany->GetNext()){
	//$arrCompanies[] = $arrCompany;
	$out[] = array(
		'value' => $arrCompany['ID'],
		'text' => iconv("cp1251", "UTF-8", $arrCompany['NAME'])
	);
}


echo json_encode($out);