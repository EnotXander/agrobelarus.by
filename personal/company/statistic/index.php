<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Статистика");
$APPLICATION->AddChainItem($APPLICATION->GetTitle());
?>

<?
$APPLICATION->IncludeComponent("bitrix:main.include", "title", Array(
    "AREA_FILE_SHOW" => "sect",
    "AREA_FILE_SUFFIX" => "title",
    "AREA_FILE_RECURSIVE" => "Y",
    "EDIT_MODE" => "html",
        ), false, Array('HIDE_ICONS' => 'Y')
);
?>

<?
if(ENABLE_PREDSTAVITEL_MODE)
{
   $arPredstavitelInfo = PredstavitelGetByUserAgro($USER->GetID());
   if($arPredstavitelInfo["RELATED"])
   {
      $element["ID"] = $arPredstavitelInfo["RELATED"];
   }
}
else
{
   if (!CModule::IncludeModule("iblock")) die();
   $objElement = new CIBlockElement();
   $res = $objElement->GetList(array(), array("IBLOCK_ID" => 65, "ACTIVE" => "Y", "PROPERTY_USER" => $USER->GetID()));
   if ($element = $res->GetNext())
   {}
}
?>

<?
//статистика
$APPLICATION->IncludeComponent("agro:metrika.statistic", "", array(
         "COMPANY_ID" => $element['ID'],
         //"COUNTER_ID" => 13147810
    ));
?> 

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>